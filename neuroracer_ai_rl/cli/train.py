#!/usr/bin/env python
import os

import numpy as np
from neuroracer_ai import ImageProcessingParameters, Image2DProcessorSuite
from neuroracer_ai.suites import SingleViewSnapshotProcessorSuite, DualViewSnapshotProcessorSuite
from neuroracer_ai.utils import config_handler
from neuroracer_ai.utils import history_logger
from neuroracer_ai.utils.arg_handler import get_argparser
from ruamel.yaml import YAML

from neuroracer_ai_rl import AI, KerasArchitectures, TrainParameters

MODEL_FILE_EXTENSION = ".hdf5"


def update_if_not_none(config, extension):
    """
    Copies the values of extension into config, if extension[key] is not None.

    :param config: The config to update.
    :type config: dict
    :param extension: config will be updated with extensions content.
    :type extension: dict
    """

    for key, value in extension.items():
        if value is not None:
            config[key] = value


def get_architecture(architecture_str):
    """
    Translates a string defining the architecture into the enum.

    :param architecture_str: A string defining which architecture is used
    :type architecture_str: str
    :returns: An architecture building function
    :rtype: Callable
    """

    if not hasattr(KerasArchitectures, architecture_str):
        raise ValueError("Architecture '{}' is unknown".format(architecture_str))

    return getattr(KerasArchitectures, architecture_str)


# A list of all config keys available
config_keys = ["train_extracted_sources", "evaluation_extracted_sources", "model_name",
               "model_dir", "mode", "num_epochs", "batch_size", "architecture", "image",
               "verbose", "dual_view", "runs", "log_file", "shuffle", "preload_data_to_ram",
               "topics", "cameras", "action"]


def create_default_config_dict():
    """
    Returns a default config dict without processing commandline
    arguments nor config files.

    :returns: A default config dict
    :rtype: dict
    """

    config = {}

    for ck in config_keys:
        config[ck] = None

    default_values = {"mode": "smart",
                      "num_epochs": 10,
                      "architecture": "htw_modified_nvidia",
                      "verbose": 1,
                      "dual_view": False,
                      "log_file": "",
                      "preload_data_to_ram": False}

    for key, value in default_values.items():
        config[key] = value

    return config


def key_in_dict(key, dictionary):
    """
    Returns True if key is in config and not None otherwise False

    :param key: The key to check.
    :type key: Any
    :param dictionary: A dictionary which couldt contain the key
    :type dictionary: dict
    :returns: True if key is in config and not None otherwise False
    :rtype: bool
    """
    return (key in dictionary) and (dictionary[key] is not None)


def check_config(config):
    """
    Checks if the config dictionary is valid

    :param config: The configuration dictionary to check
    :type config: Dict[str, Any]
    :raises KeyError: If a needed key is not specified
    """

    # check train sources
    if not key_in_dict("train_extracted_sources", config):
        raise KeyError("You have to specify '--train_extracted_sources'."
                       " Type 'train.py --help' for more information.")

    if not key_in_dict("evaluation_extracted_sources", config):
        raise KeyError("You have to specify '--evaluation_extracted_sources'."
                       "Type 'train.py --help' for more information.")

    needed_keys = ["verbose", "model_dir", "runs"]

    # check needed keys
    for needed_key in needed_keys:
        if not key_in_dict(needed_key, config):
            raise KeyError("You have to specify '--{}'."
                           "Type 'train.py --help' for more information.".format(needed_key))


def check_config_extension(extension):
    """
    Raises an KeyError if extension contains an unknown configkey.

    :param extension: A dictionary
    :type extension: dict

    :raises KeyError: If extension contains an unknown configkey
    """

    for key in extension:
        if key not in config_keys:
            raise KeyError("Unknown key found: '{}'".format(key))


def process_args(args):
    """
    Processes the arguments provided by _get_arguments().
    Returns a dictionary with the items in args but nested in the same
    way like the data in the configfile.

    :param args: args returned by _get_arguments()
    :returns: A dictionary with the same values like args, but nested in the
              same way like the configfile
    :rtype: dict
    """

    # get the arguments as dict
    args = args.__dict__

    yaml_keys = ["image"]

    args_dict = {}

    yaml = YAML()
    for key, value in args.items():
        if key in yaml_keys and value is not None:
            value = yaml.load(value)
        args_dict[key] = value

    return args_dict


def generate_config_dict():
    """
    Creates a config dict by processing default configurations,
    configfile parameters and commandline arguments.

    :raises KeyError: If configfile contains unknown keys
    """

    # default config
    config = create_default_config_dict()

    # get arguments
    args = _get_arguments()

    # configfile
    if args.configfile:
        configfile_extension = config_handler.read_config_file(args.configfile)
        check_config_extension(configfile_extension)
        config.update(configfile_extension)

    # command line arguments
    args_dict = process_args(args)
    update_if_not_none(config, args_dict)

    check_config(config)

    return config


def get_image_shape(config):
    """
    Calculates the image shape for the ai depending on the config.

    :param config: The configuration dictionary
    :type config: dict

    :returns: The image shape of the ai
    :rtype: tuple
    """

    resize = config["image"]["resize"]
    return resize["height"], resize["width"], resize["depth"]


def get_image_processing_parameters(config):
    """
    Creates the image_processing_parameters for the ai

    :param config: The configuration dictionary
    :type config: dict

    :returns: An ImageProcessingParameter object, if needed otherwise None
    :rtype: ImageProcessingParameter
    """

    if "image" in config and config["image"]:
        image = config["image"]
    else:
        return None

    def load_array_from_disk(path):
        if not os.path.exists(path):
            print('WARNING: No numpy array found under {}\n Processing Step is not being performed'.format(path))
            return None
        else:
            print('Loading {}...'.format(path))
            return np.load(path).astype('float')

    def load_from_disk_or_cast_to_float(metric):
        if isinstance(metric, str):
            return load_array_from_disk(metric)
        elif isinstance(metric, int) or isinstance(metric, float):
            return float(metric)
        else:
            return None

    # define resize parameters
    resize_parameters = None

    if "resize" in image and image["resize"]:
        resize = image["resize"]
        resize_parameters = ImageProcessingParameters.ResizeParameters(
            x_scaling=resize["width"],
            y_scaling=resize["height"],
            relative_scaling=resize["relative"])

    # define opencv_color_space_conv_func
    opencv_color_space_conv_func = None

    if "opencv_color_space_conv_func" in image:
        opencv_color_space_conv_func = image["opencv_color_space_conv_func"]

    # define flip option
    # TODO flip_option not in config, ImageProcessingParameters has no Flip()
    flip_option = None
    if "flip_option" in image:
        flip_option = ImageProcessingParameters.Flip(image["flip_option"])

    # define crop parameters
    crop_parameters = None

    if "crop" in image and image["crop"]:
        crop = image["crop"]
        crop_parameters = ImageProcessingParameters.CropParameters(
            top=crop["top"], bottom=crop["bottom"], left=crop["left"],
            right=crop["right"], relative=crop["relative"])

    # define decode_image
    decode_image = True
    if "decode_image" in image:
        decode_image = image["decode_image"]

    mean = None
    if "subtract_mean" in image:
        mean = load_from_disk_or_cast_to_float(image["subtract_mean"])

    # define normalize
    norm_factor = None
    if "normalize" in image:
        norm_factor = load_from_disk_or_cast_to_float(image["normalize"])

    # build image_processing_parameters
    image_processing_parameters = ImageProcessingParameters(
        resize_parameters=resize_parameters,
        opencv_color_space_conv_func=opencv_color_space_conv_func,
        flip_option=flip_option,
        crop_parameters=crop_parameters,
        normalize=norm_factor,
        decode_image=decode_image,
        subtract_mean=mean)

    return image_processing_parameters


def get_train_parameters(model_dir, train_extracted_sources, evaluation_extracted_sources,
                         batch_size, topics, dual_view, shuffle, preload_data_to_ram):
    train_snapshot_generator = ExtractedDataReader(main_dir=train_extracted_sources,
                                                   batch_size=batch_size,
                                                   shuffle=shuffle,
                                                   return_dict=False,
                                                   loop_indefinitely=not preload_data_to_ram,
                                                   nb_cpu=config_handler.NB_CPU)

    eval_snapshot_generator = ExtractedDataReader(main_dir=evaluation_extracted_sources,
                                                  batch_size=batch_size,
                                                  shuffle=shuffle,
                                                  return_dict=False,
                                                  loop_indefinitely=not preload_data_to_ram,
                                                  nb_cpu=config_handler.NB_CPU)

    if dual_view:
        snapshot_processor = DualViewSnapshotProcessorSuite(camera_topics=topics["cameras"],
                                                            action_topics=topics["actions"])
    else:
        snapshot_processor = SingleViewSnapshotProcessorSuite(camera_topic=topics["cameras"],
                                                              action_topics=topics["actions"])

    train_generator = (snapshot_processor.process(train_batch)
                       for train_batch in train_snapshot_generator)

    eval_generator = (snapshot_processor.process(eval_batch)
                      for eval_batch in eval_snapshot_generator)

    return TrainParameters(train_data_generator=train_generator,
                           validation_data_generator=eval_generator,
                           batch_size=batch_size,
                           steps_per_epoch=train_snapshot_generator.batches_per_epoch,
                           validation_steps=eval_snapshot_generator.batches_per_epoch,
                           checkpoint_dir=model_dir,
                           preload_data_to_ram=preload_data_to_ram)


def main():
    """
    Reads sources from disk and starts to train an AI.

    :raises ValueError: If the mode-argument is not in a valid state.
    """
    # configuration
    config = generate_config_dict()

    verbose = config["verbose"]

    runs = config["runs"]
    model_dir = config["model_dir"]

    for run in runs:

        model_name = run["model_name"]
        run_dir = os.path.join(model_dir, model_name)
        log_file = os.path.join(run_dir, 'history_log_file')

        # get data
        train_parameters = get_train_parameters(model_dir=run_dir,
                                                train_extracted_sources=config[
                                                    "train_extracted_sources"],
                                                evaluation_extracted_sources=config[
                                                    "evaluation_extracted_sources"],
                                                batch_size=run["batch_size"],
                                                dual_view=run["dual_view"],
                                                topics=config["topics"],
                                                shuffle=run["shuffle"],
                                                preload_data_to_ram=config["preload_data_to_ram"])
        # load or create ai

        if not os.path.exists(run_dir):
            os.makedirs(run_dir)

        model_file = os.path.join(run_dir, model_name + MODEL_FILE_EXTENSION)
        mode = run["mode"]

        if os.path.isfile(model_file):
            if mode == "smart":
                mode = "load"
            elif mode == "create":
                raise IOError("File '{}' already exists."
                              "If you want to keep training this model use '--mode load'"
                              "or '--mode smart' instead. Or '--mode overwrite'"
                              "to overwrite this model.".format(model_file))
        else:
            if mode == "smart":
                mode = "overwrite"
            elif mode == "load":
                raise IOError("File '{}' not found. If you want to create a new model"
                              "use another mode instead.".format(model_file))

        architecture = None

        # load ai
        if mode == "load":
            ai = AI.load(name=model_name, model_dir=run_dir)
            if verbose > 0:
                print("loading ai {}".format(os.path.join(run_dir, model_name)))
        # create ai
        elif mode == "create" or mode == "overwrite":
            if verbose > 0:
                print("creating new ai at {}".format(os.path.join(run_dir, model_name)))
            # define architecture
            architecture = get_architecture(run["architecture"])

            # define image_shape
            image_shape = get_image_shape(run)

            # define image processing parameters
            image_processing_parameters = get_image_processing_parameters(run)

            # define processor_suite
            processor_suite = Image2DProcessorSuite(
                image_processor_params=image_processing_parameters)

            ai = AI.create(
                name=run["model_name"],
                model_dir=run_dir,
                architecture=architecture,
                architecture_params={"image_shape": image_shape},
                processor_suite=processor_suite)

        else:
            raise ValueError("couldn't load ai. mode={}".format(mode))

        if ("num_epochs" in run) and (run["num_epochs"] is not None):
            train_parameters.epochs = run["num_epochs"]

        # train the ai
        history = ai.train(train_parameters, verbose=verbose)

        history_logger.log_history(history,
                                   architecture_func=architecture,
                                   model_name=model_name,
                                   destination=log_file)


def _get_arguments():
    """
    Creates the ArgumentParser and returns the parsed arguments.
    :returns: A dictionary with the parsed action line arguments.
    """

    # get common args
    parser = get_argparser(description='Reads extracted data sources and trains a machine learning model.\n'
                                       'This script is used to train machine learning models that can be used'
                                       'to control the racecar. A variety of different architectures can be trained'
                                       'using this script.')
    return parser.parse_args()


if __name__ == '__main__':
    main()
