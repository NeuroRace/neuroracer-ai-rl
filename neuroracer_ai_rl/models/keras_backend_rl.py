from neuroracer_ai.models.keras_base_backend import KerasBaseModel
from neuroracer_ai.models.abstract import Trainable


class KerasModelRL(KerasBaseModel, Trainable):
    """
    Keras reinforcement learning implementation.
    """

    def __init__(self):
        super(KerasModelRL, self).__init__()

    def train(self, train_parameters, checkpoint_path, verbose=0):
        raise NotImplementedError
