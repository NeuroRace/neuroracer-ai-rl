"""
Everything which belongs to the keras backend. TODO CHANGE FOR RL
"""
from __future__ import division

import warnings

from neuroracer_ai.models.base_keras_backend import BaseKerasModel

# hide keras future warnings as we can't change them and they bleed into our output
with warnings.catch_warnings():
    warnings.simplefilter(action="ignore", category=FutureWarning)

    import keras.layers as kl
    import keras.models as km


class KerasModel(BaseKerasModel):
    """
    Keras based AbstractModel implementation.
    """

    def __init__(self):
        super(KerasModel, self).__init__()
        self._internal_model = None  # type: km.Model

    def train(self, params, checkpoint_path, verbose=0):
        """
        Trains the model with the given data and returns the history of it.
        The data is preloaded into memory if the training parameters specify
        it that way. The Loading has to happen here, since different fit
        methods are called, depending on whether the input data is an array or
        generated in batches by a generator

        :param params: train x data
        :type params: ai.TrainParameters
        :param checkpoint_path: dir + file name for the model
        :type checkpoint_path: str
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: History object of the training
        :rtype: Dict
        """

        # TODO
        history = None

        return history


class KerasArchitectures:
    """
    Available architectures for keras models.
    """

    def __init__(self):
        pass

    @staticmethod
    def todo(image_shape):
        """
        TODO

        :param image_shape: TODO
        :type image_shape: Tuple[int, int, int]
        :return: the constructed model instance
        :rtype: keras.models.Model
        """

        model = km.Sequential()

        model.add(kl.Conv2D(
            filters=4, kernel_size=(5, 5), activation="relu", name="conv1",
            kernel_initializer="glorot_uniform",
            input_shape=image_shape))
        model.add(kl.BatchNormalization())

        model.add(kl.Conv2D(
            filters=4, kernel_size=(5, 5), activation="relu", name="conv2",
            kernel_initializer="glorot_uniform",
            input_shape=image_shape))
        model.add(kl.BatchNormalization())

        model.add(kl.Flatten(name="flattened_observation"))

        model.add(kl.Dense(
            units=150, activation="relu", kernel_initializer="glorot_uniform"))

        model.add(kl.Dense(
            units=1, kernel_initializer="glorot_uniform", name="prediction"))

        model.compile(loss="mse", optimizer="adam", metrics=["mse"])

        return model
