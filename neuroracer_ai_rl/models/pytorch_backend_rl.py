from neuroracer_ai.models.pytorch_base_backend import PyTorchBaseModel
from neuroracer_ai.models.abstract import Trainable


class PyTorchReinforcementLearningModel(PyTorchBaseModel, Trainable):
    def __init__(self):
        super(PyTorchReinforcementLearningModel, self).__init__()

    def train(self, train_parameters, checkpoint_path, verbose=0):
        # TODO: PLACEHOLDER
        pass
