"""
This module is everything a user has to import to use the library. The main api
class is AI. It contains every available call. The other classes are all helper
classes to simplify the configuration.
"""

# TODO MODIFY FOR RL

from neuroracer_ai.ai import AI as BASE_AI
from neuroracer_ai.suites import EmptyProcessorSuite
from neuroracer_ai.utils import file_handler as fh

from neuroracer_ai_rl.models import KerasModel


class TrainParameters:
    """
    Parameters which are used for the training.
    """

    def __init__(self, action_space, observation_space, environment,
                 max_steps_per_epoch, number_steps, checkpoint_dir=None,
                 use_checkpoints=True):
        """
        :param action_space: Generator returning training data and
                                     label batches
        :type action_space: Generator
        :param observation_space: Generator returning validation data
                                          and label batches
        :type observation_space: Generator
        :param environment: Size of the batches which are used. Defaults to 120
        :type environment: int
        :param max_steps_per_epoch: How many batches does the training data
                                generator have to return, before one epoch
                                is complete
        :type max_steps_per_epoch: int
        :param number_steps How many batches does the validation data
                                generator have to return, before one
                                validation epoch is complete
        :type number_steps: int
        :param checkpoint_dir: Path in which checkpoints are written
        :type checkpoint_dir: str
        :param use_checkpoints: Enables the saving of checkpoints.
                                Defaults to True
        :type use_checkpoints: bool
        """

        self.action_space = action_space
        self.observation_space = observation_space
        self.batch_size = environment
        self.steps_per_epoch = max_steps_per_epoch
        self.validation_steps = number_steps
        self.checkpoint_dir = checkpoint_dir
        self.use_checkpoints = use_checkpoints


class AI(BASE_AI):
    @staticmethod
    def load(name, model_dir, trainable=True):
        """
        Loads the given model and returns a working instance of the ai class.
        Setting trainable to false decreases the memory usage significantly!
        But should only be used if you only want to predict with an already
        trained nn.
        If you try to train an untrainable nn a Error will be thrown!

        :param: name: name of the ai/ model
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param trainable: loads the model in a way which allows it to be
                          trained.
        :type trainable: bool
        :return: constructed AI instance
        :rtype: neuroracer_ai.AI
        """

        ai_parameters = fh.load_ai_parameters(fh.create_config_path(model_dir, name))

        return AI(
            name=name,
            model_dir=model_dir,
            model=KerasModel.load(
                model_path=fh.create_model_path(model_dir, name),
                model_type=KerasModel,
                trainable=trainable),
            **ai_parameters)

    @staticmethod
    def create(name, model_dir, architecture, architecture_params,
               processor_suite=EmptyProcessorSuite()):
        """
        Creates a new model and based on it a new working instance of the ai
        class.

        :param name: name of which the ai/ model uses to save
        :type name: str
        :param model_dir: directory in which the model lives
        :type model_dir: str
        :param architecture: architecture which is used by the model
        :type architecture: Architecture
        :param architecture_params: parameters which are passed to the
                architecture constructor
        :type architecture_params: Dict[str, Any]
        :param processor_suite: processing suite which processes every input
                for training and prediction. If you need something fancier than
                the predefined ones you can use the
                neuroracer_ai.suites.ManualMappingProcessorSuite. It allows you
                to completely customize the routing of the data to the
                specified processors. You should never implement your own
                ProcessorSuite class! This will cause a failure while loading
                the model next time.
        :type processor_suite: neuroracer_ai.suites.AbstractProcessorSuite
        :return: constructed AI instance
        :rtype: neuroracer_ai.AI
        """

        return AI(
            model=KerasModel.create(
                model_type=KerasModel,
                architecture_func=architecture,
                architecture_func_params=architecture_params),
            processor_suite=processor_suite,
            name=name,
            model_dir=model_dir)

    def train(self, train_parameters, verbose=0):
        """
        Trains the model with the given data and returns the history of it.

        :param train_parameters: parameters which should be used for the
                training
        :type train_parameters: ai.TrainParameters
        :param verbose: sets the verbosity level of the model 0 = silent,
                1 = progress bar, 2 line per epoch
        :type verbose: int
        :return: History object of the training
        :rtype: Dict
        """

        if train_parameters.use_checkpoints:
            self._write_config(directory=self._model_dir, overwrite=True)

        return self._model.train(
            params=train_parameters,
            checkpoint_path=fh.create_model_path(self._model_dir, self._name),
            verbose=verbose)

    def save(self, directory, overwrite=True):
        """
        Saves the model and the ai config int the given dir.

        :param directory: directory to save
        :type directory: str
        :param overwrite: manages if model files should be overwritten
        :type overwrite: bool
        """

        self._write_config(directory=directory, overwrite=overwrite)
        self._model.save(
            path=fh.create_model_path(directory, self._name),
            overwrite=overwrite)

    def _write_config(self, directory, overwrite):
        """
        Helper method to write the internal state into a config file.
        It prepares the data for the file_handler.create_config_path method

        :param directory:
        :param overwrite:
        """

        # todo: maybe we should make this use some list which contains the field that should get serialized?
        # todo: should we also save the last train parameters?

        data = {
            "ai_parameters": {
                "processor_suite": self._processor_suite
            }
        }

        fh.write_ai_config(
            config_path=fh.create_config_path(directory, self._name),
            overwrite=overwrite,
            data=data)
