#!/usr/bin/env python

from codecs import open
from os import path
from sys import version_info

from setuptools import setup, find_packages

here = path.abspath(path.dirname(__file__))

with open(path.join(here, "README.md"), encoding="utf-8") as file_handle:
    long_description = file_handle.read()

# common dependencies
install_req = [
    "flufl.enum",  # TODO specify >= version
    "h5py>=2.6.0",
    "keras>=2.0.0",
    "keras-rl>=0.4.2",
    "keras-vis",  # TODO specify >= version
    # "neuroracer-ai>=0.3.0",
    "numpy>=1.11.1",
    "pydot>=1.1.0",
    "pymp-pypi>=0.4.0",  # TODO specify >= version
    "ruamel.yaml",  # todo: backtrace the oldest usable version
    "scikit-image>=0.12.3",
    "scikit-learn>=0.17.1",
    "scipy>=0.18.0",
    "tqdm"  # TODO specify >= version
]

# handle different lib versions for python2/3
if version_info.major < 3:
    install_req.append("matplotlib<=2.2.3")
else:
    install_req.append("matplotlib>=3.0.0")

extras_req = {
    "tf": ["tensorflow>=1.9", "tensorboard>=1.9"],
    "tf_gpu": ["tensorflow-gpu>=1.9", "tensorboard>=1.9"],
    "torch": ["torch>=1.0.1"]
}

setup(
    name="neuroracer-ai-rl",
    version="0.1.3",
    description="Reinforcement Learning AI Library for the NeuroRace Project",
    long_description=long_description,
    long_description_content_type='text/markdown',
    url="https://gitlab.com/NeuroRace",
    # author="",
    # author_email="",
    classifiers=[
        "Development Status :: 1 - ALPHA",
        "Intended Audience 2:: Developers",
        "Topic :: Utilities",
        "License :: OSI Approved :: MIT License",
        "Programming Language :: Python :: 2.7"
    ],
    keywords="ai ros autonomous",
    packages=find_packages(exclude=["docs, tests"]),
    install_requires=install_req,
    extras_require=extras_req,
    test_suite="tests",
    project_urls={
        "Bug Reports": "https://gitlab.com/NeuroRace/neuroracer-ai-rl/issues",
        "Source": "https://gitlab.com/NeuroRace/neuroracer-ai-rl",
    },
    # entry_points={"console_scripts": [
    #     "nrai-train = neuroracer_ai.cli.train:main",
    #     "nrai-convert = neuroracer_ai.cli.convert:main",
    #     "nrai-augment = neuroracer_ai.cli.augment:main",
    #     "nrai-debug = neuroracer_ai.cli.debug:main"
    # ]}
)
